var osMap;

// Our Namespace
var OpenSpace = OpenSpace || {};

OpenSpace.init = function() {
  // Only run if there are settings to use.
  if(Drupal.settings.openspace != undefined) {
	openspace_settings =   Drupal.settings.openspace;
	if(openspace_settings.admin) {
	  osMap = new OpenSpace.Map('openspace-map-sample');
	  openspace_settings.easting = openspace_settings.easting || 350000;
	  openspace_settings.northing = openspace_settings.northing || 600000;
	  openspace_settings.zoom = openspace_settings.zoom || 0;
	}
	else {
	  osMap = new OpenSpace.Map('openspace-map');
	}
	
    osMap.setCenter(new OpenSpace.MapPoint(openspace_settings.easting, openspace_settings.northing), openspace_settings.zoom);
  }
}

$(document).ready(OpenSpace.init);