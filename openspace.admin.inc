<?php

/**
 * @file
 * Provides admin functions for openspace
 */


/**
 * Returns the 'configure' $op info for hook_block().
 */
function _openspace_block_configure($delta) {
  $block_info = variable_get("openspace_block" . $delta, array());
  $form['openspace_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Block Width'),
    '#default_value' => $block_info['width'],
    '#size' => 10,
    '#description' => t('This is the width of the block.'),
  );

  $form['openspace_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Block height'),
    '#default_value' => $block_info['height'],
    '#size' => 10,
    '#description' => t('This is the height of the block.'),
  );

  $form['openspace_easting'] = array(
    '#type' => 'textfield',
    '#title' => t('Easting'),
    '#default_value' => $block_info['easting'],
    '#size' => 10,
    '#description' => t('This is the easting value for the block.'),
  );
  $form['openspace_northing'] = array(
    '#type' => 'textfield',
    '#title' => t('Northing'),
    '#default_value' => $block_info['northing'],
    '#size' => 10,
    '#description' => t('This is northing value for the block.'),
  );
  $form['openspace_zoom'] = array(
    '#type' => 'select',
    '#title' => t('Default Zoom'),
    '#default_value' => $block_info['zoom'],
    '#size' => 10,
    '#options' => array(
      '0' => t('0 Outline of Great Britian'),
      '1' => t('1 Overview of Great Britian'),
      '2' => t('2 Overview of Great Britian'),
      '3' => t('3 MiniScale®'),
      '4' => t('4 MiniScale®'),
      '5' => t('5 1:250 000 Scale Colour'),
      '6' => t('6 1:250 000 Scale Colour'),
      '7' => t('7 1:50 000 Scale Colour'),
      '8' => t('8 1:50 000 Scale Colour'),
      '9' => t('9 OS Street View®'),
      '10' => t('10 OS Street View®'),
    ),
    '#description' => t('This is inital zoom value for the block.'),
  );

  $form['openspace_sample'] = array(
    '#type' => 'item',
    '#title' => t('Sample'),
    '#suffix' => "<div id='openspace-map-sample' style='width:300px; height:500px;'></div>",
    '#description' => t('This is a sample of how the map will look when live.'),
  );


  return ($form);
}

/**
 * Returns the 'save' $op info for hook_block().
 */
function _openspace_block_save($delta, $edit) {
  $block_info = array();
  $block_info['width'] = check_plain($edit['openspace_width']);
  $block_info['height'] = check_plain($edit['openspace_height']);
  $block_info['northing'] = check_plain($edit['openspace_northing']);
  $block_info['easting'] = check_plain($edit['openspace_easting']);
  $block_info['zoom'] = $edit['openspace_zoom'];
  variable_set("openspace_block" . $delta, $block_info);
}

/**
 * Admin settings form.
 * @return unknown_type
 */
function openspace_admin_settings() {
  $form['#description'] = t('This form controls settings for the OpenSpace module, to use the module you need a username and key from the OpenSpace website:  http://openspace.ordnancesurvey.co.uk');

  $form['openspace_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('openspace_username', ''),
    '#description' => t('Username provided to you when you signed up'),
  );

  $form['openspace_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#default_value' => variable_get('openspace_key', ''),
    '#description' => t('Key provided to you when you signed up'),
  );

  $form['openspace_URL'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#default_value' => variable_get('openspace_URL', ''),
    '#description' => t('URL for OpenSpace, if this changes you can update here'),
  );

  return system_settings_form($form);
}

